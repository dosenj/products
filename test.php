<!DOCTYPE html>
<html lang="en">
	<head>
		<style>
			table, th, td {
  				border: 1px solid black;
			}
			td, th {
				padding: 10px;
				text-align: center;
			}
			th {
				background-color: lightblue;
			}
			tr:hover {
				background-color: #999;
			}
			div.data {
				margin-bottom: 50px;
				border: 2px solid black;
				padding: 20px;
			}
		</style>
	</head>
	<body>

	</body>
</html>

<?php

$languages = ['rs', 'ru', 'it', 'pt', 'es', 'de', 'en', 'fr'];

$emptyRecords = [];

foreach ($languages as $key => $value) {
	
	$connection = new mysqli('localhost', 'root', 'aurora', 'products');

	if ($connection->connect_error) {
	    die("Connection failed: " . $connection->connect_error);
	} else {
			
		$sql = "SELECT * FROM
			(SELECT DISTINCT Slug FROM product) slugs
			LEFT JOIN product ON product.Slug = slugs.Slug
			AND Language = $value";

		$results = $connection->query($sql);

		if ($results->num_rows > 0) {

			echo "<div class='data'>";

			echo "<table>";

			echo "<tr><th>ID</th><th>SLUG</th><th>TITLE</th><th>EXCERPT</th><th>IMAGE</th><th>ACTIVE</th><th>DELETED</th><th>LANGUAGE</th></tr>";
	   
		    while($row = $results->fetch_object()) {

		    	if( is_null($row->ID) ){
		    		$emptyRecords[] = $row;
		   		} else {
		   			echo "<tr><td>".$row->ID."</td><td>".$row->Slug."</td><td>".$row->Title."</td><td>".$row->Excerpt."</td><td>".$row->Image."</td><td>".$row->Active."</td><td>".$row->Deleted."</td><td>".$row->Language."</td></tr>";
		   		}

		    }

		    echo "</table>";

		    echo "<div>";

		} else {
			echo "No records found!";
		}

		$connection->close();

	}

}