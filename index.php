<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Test</title>
		<style>
			table, th, td {
  				border: 1px solid black;
			}
			td, th {
				padding: 10px;
				text-align: center;
			}
			th {
				background-color: lightblue;
			}
			tr:hover {
				background-color: #999;
			}
			div.data {
				margin-bottom: 250px;
				padding: 20px;
				width: 750px;
			}
		</style>
	</head>
	<body>
		<?php

			$connection = new mysqli('localhost', 'root', 'aurora', 'products');

			if ($connection->connect_error) {
			    die("Connection failed: " . $connection->connect_error);
			} else {
					
				$sql = "SELECT * 
						FROM ( 
						    SELECT Slug, 
						    MAX(ID) AS ID,
						    MAX(Title) AS Title,
						    MAX(Excerpt) AS Excerpt,
						    MAX(Image) AS Image,
						    MAX(Active) AS Active,
						    MAX(Deleted) AS Deleted,
						    MAX(Language) AS Language
						    FROM product
						    WHERE Deleted = 0 AND Title IS NOT NULL AND Title <> ''
						    GROUP BY Slug 
						) x";

				$results = $connection->query($sql);

				$total = $results->num_rows;

				$slugs = [];

				$records = [];

				if ($total > 0) {
			   
				    while($row = $results->fetch_object()) {

			   			$slugs[] = $row->Slug;

			   			$records[] = $row;
				   			
				    }

				} else {
					echo "No records found!";
				}

				$connection->close();
			}

		    //

		    $connection2 = new mysqli('localhost', 'root', 'aurora', 'products');

			if ($connection2->connect_error) {
			    die("Connection failed: " . $connection2->connect_error);
			} else {

				$lastElement = end($slugs);

			   	$in = "(";

			   	foreach ($slugs as $key => $value) {

			   		if( $value != $lastElement ){
			   			$in.="'$value', ";
			   		}
			   		
			   		if( $value == $lastElement ){
			   			$in.="'$value'";
			   		}

			   	}

			   	$in.= ")";
					
				$sql2 = "
						SELECT * 
						FROM ( 
						    SELECT Slug, 
						    MAX(ID) AS ID,
						    MAX(Title) AS Title,
						    MAX(Excerpt) AS Excerpt,
						    MAX(Image) AS Image,
						    MAX(Active) AS Active,
						    MAX(Deleted) AS Deleted,
						    MAX(Language) AS Language
						    FROM product
						    WHERE Slug IN$in AND Deleted = 0 AND Title IS NOT NULL AND Title <> '' AND Language = 'en'
						    GROUP BY Slug 
						) x";

				$results2 = $connection2->query($sql2);

				$total2 = $results2->num_rows;

				$slugs2 = [];

				$records2 = [];

				if ($total2 > 0) {
			   
				    while($row = $results2->fetch_object()) {

			   			$slugs2[] = $row->Slug;

			   			$records2[] = $row;
				   			
				    }

				} else {
					echo "No records found!";
				}

				$connection2->close();

			}

			$finalResults = [];

			$foo = [];

			foreach ($records as $key => $value) {
		    		
	    		foreach ($records2 as $k => $v) {

	    			if($value->Slug == $v->Slug){

    					$finalResults[] = $v;

    					$foo[$value->Slug] = $v;

    					break 1;

	    			} else {

	    				if( !isset($foo[$value->Slug]) ){
	    					$foo[$value->Slug] = $value;	
	    				}

	    			} 
	    			
	    		}

	    	}

	    	echo "<div class='data'>";

			echo "<table>";

			echo "<tr><th>ID</th><th>SLUG</th><th>TITLE</th><th>EXCERPT</th><th>IMAGE</th><th>ACTIVE</th><th>DELETED</th><th>LANGUAGE</th></tr>";

	    	foreach ($foo as $key => $value) {
	    		
	    		echo "<tr><td>".$value->ID."</td><td>".$value->Slug."</td><td>".$value->Title."</td><td>".$value->Excerpt."</td><td>".$value->Image."</td><td>".$value->Active."</td><td>".$value->Deleted."</td><td>".$value->Language."</td></tr>";

	    	}

	    	echo "</table>";
		 
		    echo "<div>";

		?>
	</body>
</html>